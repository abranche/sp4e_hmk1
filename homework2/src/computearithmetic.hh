#ifndef HOMEWORK2_COMPUTEARITHMETIC_H
#define HOMEWORK2_COMPUTEARITHMETIC_H

#include "series.hh"

class ComputeAlgebraic : public Series {
public:
  ComputeAlgebraic();
  double getAnalyticPrediction() override;
};

#endif //HOMEWORK2_COMPUTEARITHMETIC_H