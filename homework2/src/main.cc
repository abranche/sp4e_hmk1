/* -------------------------------------------------------------------------- */

#include <cstdlib>
#include <iostream>
#include <memory>

#include "main.hh"
#include "computepi.hh"
#include "computearithmetic.hh"
#include "printseries.hh"
#include "writeseries.hh"

using namespace std;

/* -------------------------------------------------------------------------- */

int main(int argc, char ** argv) {

    std::string calc_series = argv[0];
    std::string DumperType = argv[1];
    int freq = atoi(argv[2]);
    int N = atoi(argv[3]);
    int Preci = atoi(argv[4]);
    std::string delimiter = argv[5];

    if (DumperType == "print"){
        std::cout << "You have chosen to print your results" << std::endl;
    }
    else {
        std::cerr << "Accepted options for seeing results: print | wfile (write to file)." << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<Series> series = nullptr;
    //Check input and calculate series
    std::unique_ptr<Series> Series = nullptr;
    if (calc_series == "AR"){
        series = std::make_unique<ComputeAlgebraic>();
    }
    else if (calc_series == "PI"){
        series = std::make_unique<ComputePi>();
    }
    else {
        std::cerr << "Accepted options for series type: AR (arithmetic) | PI (pi). Please re enter args" << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<DumperSeries> dumper = nullptr;
    //Print results
    if (DumperType == "print") {
        dumper = std::make_unique<PrintSeries>(*series, freq);
    }
    else if (DumperType == "write"){
        dumper = std::make_unique<WriteSeries>(*series, delimiter);
    }

    std::ostream *file = nullptr;
    file = &std::cout;

    dumper->setPrecision(Preci);
    dumper->setMaxIter(N);
    *file << *dumper << std::endl;

    return EXIT_SUCCESS;
}

