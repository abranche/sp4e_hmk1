### SP4E Homework 2
This repo contains the code to solve the exercises proposed for Homework 2.
Its authors are:

Shayan Khalooei (shayan.khalooei@epfl.ch)\
Pedro Abranches de Carvalho (pedro.abranches@epfl.ch)

##### Arguments

* argv[0] is the series type: pi or arithmetic
* argv[1] is the dumper type: print or write
* argv[2] is the output frequency of the serie value
* argv[3] is series number
* argv[4] is the precision
* argv[5] is the delimiter

#### Run the code:

mkdir build
cd build
ccmake ../
make
./src/main series_type dumper_type frequency series_number precision delimiter

##### Exercises
###### Ex.2.1. 
Q: What is the best way/strategy to divide the work among yourselves?\
A: Dividing the work by the series and dumper class seems the best way to divide the work. This way, we can solve the problems referent to each class and if one needs to use any of the classes it only cares about the class itself and not the full implementation.
###### Ex.5.1.  
Q: Global complexity of the program?\
A: It should have a complexity of around O(n*(n+1)/2), given we repeated the sums, so we would calculate something as n+(n-1)+...+1 times
###### Ex.5.4.  
Q: Global complexity of the program after preventing re computation?\
A: It should have a complexity of around O(n), given we do not repeat the sums so we go through the loop reusing previous calculations
###### Ex.5.4.  
Q: Best complexity?\
A: The second one?!