import os
from scipy.optimize import minimize
from optimization.utils import function_s1, ensure_array, ensure_A_b, get_function_vals, build_mesh_grids, plot_solver_name

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def minimizer(x, A, b, solver, function=function_s1):
    """Scipy minimizer function. To be used to minimize a specific (per example quadratic) function

    :param x: x initial guess
    :param A: A matrix
    :param b: b vector
    :param solver: solver to be used
    :return: optimize result, list of iterations x coordinates, list of iterations y coordinates, function values at iterations
    """
    x = ensure_array(x)
    A, b = ensure_A_b(A, b)

    save_x = []
    save_y = []
    save_xy = []

    def checkpoint(minimizer):
        x, y = minimizer
        save_x.append(x)
        save_y.append(y)
        save_xy.append(minimizer)

    res = minimize(function_s1, x0=x, args=(A, b), method=solver, callback=checkpoint)
    func_vals = get_function_vals(function_s1, save_xy)
    return res, save_x, save_y, func_vals

def minimizer_plot(save_x, save_y, func_vals, solver, show_plot=True, save=False, save_dir=None, function=None):
    """ General plot function for different solvers and functions

    :param save_x: list of x coordinates
    :param save_y: list of y coordinates
    :param func_vals: list of quadratic function values
    :param solver: solver used
    :param save: boolean if user wishes to save
    :param save_dir: save directory
    :return: figure
    """
    fig = plt.figure(figsize=(10, 8))
    ax = fig.add_subplot(111, projection='3d')
    x_mesh, y_mesh, z_mesh = build_mesh_grids(save_x, save_y, function=function)

    ax.plot_surface(x_mesh, y_mesh, z_mesh, cmap='YlGnBu_r', alpha=0.25, zorder=0)
    ax.contour(x_mesh, y_mesh, z_mesh, 15, colors='#000000', alpha=0.3, zorder=1)
    ax.plot(save_x, save_y, func_vals, marker='o', linestyle='--', color='red', zorder=2)

    ax.set_xlabel("$x$", fontsize=12)
    ax.set_ylabel("$y$", fontsize=12)
    ax.set_zlabel('$f(x)$', fontsize=12)
    plot_sv_name = plot_solver_name(solver)
    ax.set_title(plot_sv_name, fontsize=24, y=-0.07, x=0.65)
    if save:
        if save_dir != '':
            os.makedirs(save_dir, exist_ok=True)
        plt.savefig(os.path.join(save_dir, plot_sv_name + ".pdf"))
    if show_plot:
        plt.show()

    return fig



