import argparse
import numpy as np

def function_s1(x, A=None, b=None):
    """Implementation of the quadratic function
    S(x) = x^T * A * x - x^T * b'

    :param x: x vector
    :param A: A matrix
    :param b: b vector
    :return: Value at specific point
    """
    A, b = ensure_A_b(A, b)
    return np.dot(np.dot(x.T, A), x) - np.dot(x.T, b)

def function_s2(x, A=None, b=None):
    """Implementation of the quadratic function
    S(x) = 1/2 x^T * A * x - x^T * b'

    :param x: x vector
    :param A: A matrix
    :param b: b vector
    :return: Value at specific point
    """
    A, b = ensure_A_b(A, b)
    return (1/2) * np.dot(np.dot(x.T, A), x) - np.dot(x.T, b)

def ensure_array(x):
    """Function to ensure x is array

    :param x: x list or array
    :return: assured array
    """
    return np.asarray(x).T


def ensure_A_b(A=None, b=None):
    """Ensure both A and b have values and are arrays

    :param A: A matrix
    :param b: b vector
    :return: Array A and b with meaningful values
    """
    if A is None:
        A = np.array([[4, 0], [1, 3]])
    if b is None:
        b = np.array([0, 1]).T
    A = ensure_array(A)
    b = ensure_array(b)
    return A, b


def get_function_vals(function, save_vals):
    """Get values of a specific function

    :param function: function to use
    :param save_vals: list of save coordinates
    :return:
    """
    func_vals = []
    for v in save_vals:
        func_vals.append(function(v).item())

    return func_vals


def build_mesh_grids(save_x, save_y, function=None):
    """Build grid for plotting

    :param save_x: Coordinates of x
    :param save_y: Coordinates of y
    :param function: Optimized function values
    :return:
    """
    max_x = np.max(np.abs(save_x)) * 1.3
    max_y = np.max(np.abs(save_y)) * 1.3

    x_axis = np.linspace(-max_x, max_x, 40)
    y_axis = np.linspace(-max_y, max_y, 40)
    x_mesh, y_mesh = np.meshgrid(x_axis, y_axis)

    z_mesh = np.zeros((len(x_axis), len(y_axis)))
    for i, x in enumerate(x_axis):
        for j, y in enumerate(y_axis):
            z_mesh[i, j] = function(np.array([x, y]))

    return x_mesh, y_mesh, z_mesh

def plot_solver_name(solver):
    """Get the a meaningful plotting name for a solver

    :param solver: solver used
    :return: plotting name
    """
    if solver == 'BFGS':
        plot_name = solver
    elif solver == "CG":
        plot_name = "Conjugate Gradient"
    elif solver == "own_CG":
        plot_name = "Conjugate Gradient (Own)"

    return plot_name

def str2bool(v):
    """Transform string to boolean in argparse

    :param v: string
    :return: boolean
    """
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', '1'):
        return True
    elif v.lower() in ('no', 'false', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')