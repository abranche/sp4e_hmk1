### SP4E Homework 1
This repo contains the code to solve the exercises proposed for Homework 1.
It's authors are:

Shayan Khalooei (shayan.khalooei@epfl.ch)\
Pedro Abranches de Carvalho (pedro.abranches@epfl.ch)

##### Packages needed
1. python 3.7.9
2. scipy 1.4.1
3. numpy-base 1.19.1
4. matplotlib-base 3.3.2

#### Running the code
In order to make matters easy we have an unique main file. 
To run the code, one should be in the `sp4e_hmk1` directory:
1. Run as `python main.py --solver=CG` to run the different optimizers (here the scipy Conjugate Gradient is chosen).

One can append more commands to this basic one:
1. `--plot=True` allows the user plot the figure (can be used for debugging)
2. `--save_plot=True` allows the user to also save the plot or with `--save_dir=plots` to give it a directory
3. `--show_plot=True` allows the user to see the plot

Use `python main.py --help` to see all the possible commands (the above ones are the most useful).
#### Exercises and where to find them
Ex.1.1 --> inside the optimization folder one can find the `optimization.py` with the `minimizer` routine.\
Ex.1.2 --> inside the same file as Ex1.1 there is also the `minimizer_plot` routine for the plotting.\
Ex.2.1 --> inside the CG folder, the `conjugate_gradient.py` has our own implementation of Conjugate Gradient.\
Ex.2.2 --> can be done by running `python main.py --solver=own_CG --data=True`, where A and b values are overwritten. One can use their own files or change by hand the ones given.\
Ex.2.3 --> can be done by running `python main.py --solver=own_CG --plot=True --show_plot=True` and changing the `solver` to `--solver=CG` as well.