import numpy as np
from optimization.utils import ensure_array, ensure_A_b, function_s2, get_function_vals
# Conjugate Gradient solver
def solver(x0, A, b, tol=1e-6, max_iteration=100, function=function_s2):
    """

    :param x0:
    :param A:
    :param b:
    :param tol:
    :param max_iteration:
    :param function:
    :return:
    """

    # Ensure x and A values
    x0 = ensure_array(x0)
    A, b = ensure_A_b(A, b)

    # Create lists to save iteration values
    save_x = []
    save_y = []
    save_xy = []
    def checkpoint(minimizer):
        x, y = minimizer
        save_x.append(x)
        save_y.append(y)
        save_xy.append(minimizer)
    checkpoint(x0)

    # Start optimization
    x_old = x0
    r_old = b - np.einsum('ij,j->i', A, x0)
    p_old = r_old
    n_iter = 0
    if np.sqrt(np.einsum('i,i->', r_old.transpose(), r_old)) < tol:
        return x_old, n_iter
    n_iter += 1

    while n_iter <= max_iteration:

        Ap = np.einsum('ik,k->i', A, p_old)
        alpha = np.einsum('i,i->', r_old.transpose(), r_old) / np.einsum('k,k->', p_old.transpose(), Ap)
        x_new = x_old + alpha * p_old
        r_new = r_old - alpha * Ap

        # Save iterations
        checkpoint(x_new)

        if np.sqrt(np.einsum('i,i->', r_new.transpose(), r_new)) < tol:
            break

        beta = np.einsum('i,i->', r_new.transpose(), r_new) / np.einsum('i,i->', r_old.transpose(), r_old)
        p_old = r_new + beta * p_old
        r_old = r_new
        x_old = x_new
        n_iter += 1

    # Get value of function at iterations for plotting
    func_vals = get_function_vals(function, save_xy)

    return x_new, save_x, save_y, func_vals
