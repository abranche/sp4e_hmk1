import numpy as np
import argparse
import math
from CG.conjugate_gradient import solver as CG
from optimization.optimizer import minimizer, minimizer_plot
from optimization.utils import function_s1, function_s2, str2bool

##############################################################################
# Parsing the input arguments
##############################################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='SP4E Hmk 1')
    parser.add_argument('--x', nargs=2, default=[2, 3],
                        help='Initial guess for x parameter')
    parser.add_argument('--data', type=str2bool, default=False,
                        help='Overwride A and b values | Change csv files or upload desired files')
    parser.add_argument('--solver', type=str, required=True, choices=['BFGS', 'CG', 'own_CG'],
                        help='Solver used in scipy minimizer. BFGS & CG are scipy solvers | own_CG is our own implementation of CG')
    parser.add_argument('--plot', type=str2bool, default=False,
                        help='Plot iteration values')
    parser.add_argument('--save_plot', type=str2bool, default=False,
                        help='Save plot image')
    parser.add_argument('--show_plot', type=str2bool, default=False,
                        help='Show plot image')
    parser.add_argument('--save_dir', default='', help='Directory to save plot image')
    args = parser.parse_args()

    # ##############################################################################
    # # Parsing the input data
    # ##############################################################################
    # Data referent to A and b
    if args.data:
        args.A = np.loadtxt('A_matrix.csv', delimiter=',')
        args.b = np.loadtxt('b_vector.csv', delimiter=',')
    else:
        args.A = None
        args.b = None

    # ##############################################################################
    # # Minimizing procedure using selected method
    # ##############################################################################
    print("Running optimization procedure with {}".format(args.solver))
    if args.solver == "own_CG":
        function = function_s2
        res, save_x, save_y, func_vals = CG(args.x, args.A, args.b, function=function)
    else:
        function = function_s1
        res, save_x, save_y, func_vals = minimizer(args.x, args.A, args.b, args.solver, function=function)

    # ##############################################################################
    # # Plotting results
    # ##############################################################################
    if args.plot is True:
        minimizer_plot(save_x, save_y, func_vals, solver=args.solver, show_plot=args.show_plot,
                       save=args.save_plot, save_dir=args.save_dir, function=function)
